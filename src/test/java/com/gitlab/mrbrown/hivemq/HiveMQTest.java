package com.gitlab.mrbrown.hivemq;

import java.util.*;

import com.hivemq.client.mqtt.mqtt5.*;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.junit.*;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy;

import static java.nio.charset.StandardCharsets.UTF_8;

public class HiveMQTest {

    @Rule
    public GenericContainer<?> mosquitto = new GenericContainer<>("eclipse-mosquitto").withExposedPorts(1883).waitingFor(new HostPortWaitStrategy());

    private List<String> messages = new ArrayList<>();

    private MqttClient subscriber;

    public String getAddress() {
        final String host = mosquitto.getHost();
        final int port = mosquitto.getFirstMappedPort();
        return "tcp://" + host + ":" + port;
    }

    @Before
    public void setUp() throws Exception {
        subscriber = new MqttClient(getAddress(), UUID.randomUUID().toString());
        subscriber.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
            }

            @Override
            public void messageArrived(String t, org.eclipse.paho.client.mqttv3.MqttMessage m) throws Exception {
                System.out.println(new String(m.getPayload()));
                messages.add(new String(m.getPayload()));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken t) {
            }
        });

        subscriber.connect();
        subscriber.subscribe("#");
    }

    @After
    public void tearDown() throws Exception {
        subscriber.disconnect();
        subscriber.close();
    }


    @Test
   public void testHiveMQ() throws Exception {

        Mqtt5BlockingClient client = Mqtt5Client.builder()
                .identifier(UUID.randomUUID().toString())
                .serverHost(mosquitto.getHost())
                .serverPort(mosquitto.getFirstMappedPort())
                .buildBlocking();

        client.connect();
        client.publishWith()
                .topic("world/hivemq")
                .payload("Hello World from HiveMQ".getBytes(UTF_8))
                .send();
        client.disconnect();

        Thread.sleep(200);
        Assert.assertFalse(messages.isEmpty());
        Assert.assertEquals(messages.get(0), "Hello World from HiveMQ");
    }

}
